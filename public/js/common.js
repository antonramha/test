$(document).ready(function($){

  $('input[name="params.dateOfBirth"]').datetimepicker({pickTime: false});
  $('input[name="params.idIssueDate"]').datetimepicker({pickTime: false});

  //$('input[name="params.phones"]').inputmask({"mask": "(999) 999-9999"});

  $('#periodStart').datetimepicker(
    {pickTime: false}
  );
  $('#periodEnd').datetimepicker(
    {pickTime: false}
  );

  $("#periodStart").on("dp.change",function (e) {
     $("#periodEnd").data("DateTimePicker").setMinDate(e.date);
   });
   $("#periodEnd").on("dp.change",function (e) {
     $("#periodStart").data("DateTimePicker").setMaxDate(e.date);
   });

  function phoneValidate(phone){
      var pattern = /^\d{10}$/;
      return pattern.test(phone);
  }

  $('[name="params.phones"]').tagsinput();

  $('[name="params.phones"]').on('itemAdded', function(event) {
    console.log(event.item);
    if(!/\d+/.test(event.item)){
      $('[name="params.phones"]').tagsinput('remove', event.item);
    }
  });

  $("a.toolTip").tooltip();



  $('.accordion-wrap article').hide();
  $('.accordion-wrap .accordion-string').click(function () {

  	var findArticle = $(this).next('article');
  	var findWrapper = $(this).closest('.accordion-wrap');

  	if(findArticle.is(':visible')){

  	    findArticle.slideUp();

  	} else {

  	    findWrapper.find('article').slideUp();
  	    findArticle.slideDown();

  	}

  });






});
