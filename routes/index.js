var express = require('express');
var router  = express.Router();
var libs    = require('../libs');
var Voc     = require('./voc.js');

var PARAMS = [
      "dateConsentGiven",
      "lastName",
      "firstName",
      "middleName",
      "dateOfBirth",
      "placeOfBirth",
      "gender",
      "limit",
      "months",
      "phones",

      "idSeria",
      "idNumber",
      "idIssueDate",
      "idIssuePlace",
      "idIssueBy",
      "snils",

      "regRegion",
      "regCity",
      "regStreet",
      "regHouse",
      "regBuilding",
      "regAppartment",

      "actRegion",
      "actCity",
      "actStreet",
      "actHouse",
      "actBuilding",
      "actAppartment",

      "loan",
      "loanDuration",
      "loanType",
];

/* GET home page. */
router.get('/', function(req, res, next) {

  var data = {
                  voc: Voc,
                  sources: [
                    {
                      code: "skipTrace",
                      name: "Проверка телефона",
                      enabled: true,
                      price: 0
                    },
                    {
                      code: "fastCheck",
                      name: "Быстрая проверка",
                      enabled: true,
                      price: 0
                    },
                    {
                      code: "longCheck",
                      name: "Не очень быстрая",
                      enabled: true,
                      price: 0
                    },


                  ],
                  params: {
                      consentGivenDate: libs.date2str(new Date())
                  }
              }


  res.render('index', data );



});

router.get('/history', function(req, res, next) {

  var data = {
    name: 'username',
  }


  res.render('history', data );

});


router.post('/create', function (req, res, next) {
  var body = req.body;
  //var userName = req.session.username;
  var params = {
      //sources
      fastCheck: body["fastCheck"],
      skipTrace: body["skipTrace"],
      longCheck: body["longCheck"],

      lastName: body["params.lastName"],
      firstName: body["params.firstName"],
      middleName: body["params.middleName"],
      dateOfBirth: body["params.dateOfBirth"],
      placeOfBirth: body["params.placeOfBirth"],
      gender: body["params.gender"],
      months: body["params.months"],
      limit: body["params.limit"],
      phones: body["params.phones"],

      idSeria: body["params.idSeria"],
      idNumber: body["params.idNumber"],
      idIssueDate: body["params.idIssueDate"],
      idIssuePlace: body["params.idIssuePlace"],
      idIssueBy: body["params.idIssueBy"],
      snils: body["params.snils"],

      regRegion: body["params.regRegion"],
      regCity: body["params.regCity"],
      regStreet: body["params.regStreet"],
      regHouse: body["params.regHouse"],
      regBuilding: body["params.regBuilding"],
      regAppartment: body["params.regAppartment"],

      actRegion: body["params.actRegion"],
      actCity: body["params.actCity"],
      actStreet: body["params.actStreet"],
      actHouse: body["params.actHouse"],
      actBuilding: body["params.actBuilding"],
      actAppartment: body["params.actAppartment"],

      loan: body["params.loan"],
      loanDuration: body["params.loanDuration"],
      loanCurrency: body["params.loanCurrency"],
      loanReason: body["params.loanReason"],
      loanType: body["params.loanType"],
  }

  res.send({
                success: true,

            });

});

module.exports = router;
