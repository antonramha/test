
function copyField (field) {
    return {
        name: field.name,
        path: field.path,
        tag: field.tag,
        title: field.title,
        type: field.type,
        options: field.options,
        mondatory: field.mondatory,
        sources: field.sources
    };
}

module.exports = function (fields) {
    
    return function Form () {
        var params = fields.map(copyField);
        
        return {
            handle: function (data) {
                params.forEach(function (field) {
                    if (data.hasOwnProperty(field.name) && data[field.name] !== "") {
                        field.value = data[field.name];
                    }
                });
            },
            filterFields: function (callback) {
                params = params.filter(callback);
            },
            getFieldsByName: function () {
                var out = {};
                params.forEach(function(field) {
                    out[field.name] = field;
                });
                return out;
            },
            getValuesByName: function () {
                var out = {};
                params.forEach(function (field) {
                    out[field.name] = field.value;
                });
                return out;
            },
            setErrors: function (validationError) {
                var errors = validationError.errors;
                params.forEach(function (field) {
                    var err = errors[field.path];
                    if (err) {
                        if (err.kind="required") {
                            field.error = 'Поле "'+field.title+'" обязательно для заполнения';
                        } else {
                            field.error = 'Неверное значение поля "'+field.title+'"';
                        }
                        field.errorType = err.kind;
                    }
                });
            }
        }
    }
};
