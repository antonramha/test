var exec = require("child_process").exec,
    path = require("path");

function msxsl (xml, xsl, out, cb) {
    if (typeof xml !== "string") {
        process.nextTick(cb, new Error("Неверный ип параметра xml"));
        return;
    }
    if (typeof xsl !== "string") {
        process.nextTick(cb, new Error("Неверный ип параметра xsl"));
        return;
    }
    if (typeof cb !== "function") {
        cb = function () {}
    }
    var xmlPath = path.resolve(xml),
        xslPath = path.resolve(xsl);

    exec(`C:\\mfoservice.new\\msxsl.exe "${xmlPath}" "${xslPath}" -o "${out}"`, cb);
}

module.exports = {
    transform: msxsl
}