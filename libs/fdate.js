
var replaces = {
    DD: function (date) {
        var day = date.getDate();
        return (day < 10 ? "0" : "") + day;
    },
    MM: function (date) {
        var month = date.getMonth();
        return (month < 10 ? "0" : "") + (month + 1);
    },
    YY: function (date) {
        return date.getFullYear();
    }
}

module.exports = function fdate (date, format) {
    return format.replace(/(\w+)/g, function (key) {
        if (replaces[key]) {
            return replaces[key](date);
        }
    });
}