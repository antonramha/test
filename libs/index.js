function clamp(value, min, max) {
    if (min >= max) {
        throw new Error("аргумент min должен быть меньше max");
    }
    if (!Number(value)) {
        value = 0;
    }
    if (value < min) {
        value = min;
    }
    if (value > max) {
        value = max;
    }
    return value;
}

function str2date(val) {
    //console.log("str2date:", typeof val, "=", val);
    if (typeof val === "string" && val.trim().length === 10) {
        return new Date(val.replace(/(\d+)\.(\d+)\.(\d+)/, "$2-$1-$3"));
    } else {
        return undefined;
    }
}

function date2str(date) {
    //console.log(date);
    if (date instanceof Date) {
        return (date.getDate() < 10 ? "0" : "") + date.getDate() + "." + (date.getMonth() < 9 ? "0" : "") + (date.getMonth()+1) + "." + date.getFullYear();
    } else {
        return "";
    }
}

function str2float(str) {
    if (typeof str === "string") {
        var num = parseFloat(str.replace(",", "."));
        if (num) {
            num = num.toFixed(2);
            //console.log(num);
            return num;
        }
    }
    return undefined;
}

function float2str(val) {
    if (typeof val === "number") {
        var str = val.toFixed(2).toString().replace(".", ",");
        //console.log(str);
        return str;
    }
    return undefined;
}


function errorsByPath(validationError) {
    var errors = {}
    if (!validationError.errors) {
        return errors;
    }
    for (var path in validationError.errors) {
        if (validationError.errors.hasOwnProperty(path)) {
            var pathError = validationError.errors[path];
            errors[path] = {
                kind: pathError.kind,
                message: pathError.message,
                value: pathError.value,
            }
        }
    }
    return errors;
}

module.exports = {
    logger: require('./logger.js'),
    CmtForm: require("./cmtform.js"),
    CmtPages: require("./cmtpages.js"),
    regions: require("./regions.js"),
    fdate: require('./fdate.js'),
    clamp: clamp,
    str2date: str2date,
    date2str: date2str,
    str2float: str2float,
    float2str: float2str,
    errorsByPath: errorsByPath,
};