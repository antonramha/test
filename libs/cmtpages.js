/**
 * Класс для реализации постраничного вывода
 * @param {number} itemsCount Общее количество элементов
 * @param {number} itemsPerPage Количество элементов на страницу
 * @param {number} currentPage Текущая страница
 */
function CmtPages (itemsPerPage, currentPage) {
    
    if (!Number.isInteger(itemsPerPage) || itemsPerPage < 1) {
        itemsPerPage = 10;
    }
    if (!Number.isInteger(currentPage) || currentPage < 1) {
        currentPage = 1;
    }
    
    var pages = [],
        itemsCount = 0,
        pagesCount = 0;
    
    function calcPages() {
        pages = [];
        pagesCount = Math.ceil(itemsCount/itemsPerPage);
        
        if (pagesCount < 1) {
            pagesCount = 1;
        }

        if (currentPage > pagesCount) {
            currentPage = pagesCount;
        }

        var firstPage = currentPage - 5,
            lastPage =  currentPage + 5;
        
        if (firstPage < 1) {
            firstPage = 1;
            lastPage = 11;
        }
        
        if (lastPage > pagesCount) {
            lastPage = pagesCount;
            firstPage = lastPage - 11;
            if (firstPage < 1) {
                firstPage = 1;
            }
        }
        
        // console.log('itemsCount', itemsCount);
        // console.log('pagesCount', pagesCount);
        // console.log('itemsPerPage', itemsPerPage);
        // console.log('currentPage', currentPage);
        // console.log('firstPage', firstPage);
        // console.log('lastPage', lastPage);
        
        for (var i = firstPage; i <= lastPage; i++) {
            pages.push(i);
        }
    }

    
    return {
        setItemsCount: function (count) {
            if (!Number.isInteger(count) || itemsCount < 0) {
                itemsCount = 0;
            } else {
                itemsCount = count;
            }
            calcPages();
        },
        /**
         * Получить текущую страницу
         */
        getCurrent: function () {
            return currentPage;
        },
        /**
         * Получить расчитаное общее количестово страниц
         */
        getPagesCount: function () {
            return pagesCount;
        },
        /**
         * Получить количество пропускаемых элементов
         * для текущей страницы
         */
        getSkip: function () {
            return (currentPage - 1) * itemsPerPage;
        },
        /**
         * Получить количество элементов на страницу
         * синоним itemsPerPage
         */
        getLimit: function () {
            return itemsPerPage;
        },
        /**
         * Получить массив номеров страниц относительно
         * текущей страницы.
         */
        getPages: function () {
            return pages;
        }
    }
}

module.exports = CmtPages;