
function CmtForm (schema, fields) {
    
    //отбор полей только для текущей схемы
    var fields = fields.filter(function (field) {
        return (schema.path(field.path)) ? true : false;
    });
    
    //копирование полей схемы из глоб. массива во
    //внутреннюю переменую
    fields = fields.map(function (field) {
        return {
            name: field.name,
            path: field.path,
            tag: field.tag,
            title: field.title,
            type: field.type,
            options: field.options,
            mondatory: field.mondatory,
            value: "",
        };
    });
    
    //интерфейс для работы с формой
    return {
        //обработать req.
        //извлекает значения переданные в пост-запросе
        //и сохранят его в поле как атрибут value
        handle: function (req) {
            var body = req.body;
            fields.forEach(function (field) {
                var value = body[field.name];
                if (typeof value === "string") {
                    field.value = value;
                } else {
                    field.value = "";
                }
            });
        },
        eachField: function (callback) {
            return fields.forEach(callback);
        },
        loadDoc: function (doc) {
            fields.forEach(function (field) {
                field.value = doc.get(field.path);
            });
        },
        fillDoc: function (doc) {
            var values = {};
            fields.forEach(function (field) {
                values[field.path] = field.value;
            });
            schema.eachPath(function (path) {
                if (values.hasOwnProperty(path)) {
                    doc.set(path, values[path]);
                }
            });
        },
        //получить поля по их имени
        getFieldsByName: function () {
            var obj = {};
            fields.forEach(function (field) {
                obj[field.name] = field;
            });
            return obj;
        },
        getTags: function () {
            var obj = {};
            fields.forEach(function (field) {
                obj[field.tag] = true;
            });
            return obj;
        },
        //Получить значения полей формы
        //в качестве ключа - путь к полю указаный в
        //атрибуте path.
        getValuesByPath: function () {
            var obj = {};
            fields.forEach(function (field) {
                obj[field.path] = field.value;
            });
            return obj;
        },
        setErrors: function (validationError) {
            var errors = validationError.errors;
            fields.forEach(function (field) {
                var err = errors[field.path];
                if (err) {
                    if (err.type="required") {
                        field.error = 'Поле "'+field.title+'" обязательно для заполнения';
                    } else {
                        field.error = 'Неверное значение поля "'+field.title+'"';
                    }
                    field.errorType = err.type;
                }
            });
        }
    }
}

module.exports = CmtForm;